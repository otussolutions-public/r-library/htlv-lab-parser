# HTLV Laboratory Parser

*Package* [R](https://www.r-project.org/) para converter resultados de laboratório do projeto HTLV do formato Excel (`.xlsx`) para formato JSON (`.json`) compatível com os modelos de dados da Plataforma Otus.

## Instalação

Para instalar o pacote em sua máquina, utilize o comando abaixo _em uma sessão R_:

```
remotes::install_gitlab("otussolutions-public/r-library/htlv-lab-parser")
```

Em seguida, será possível utilizar as funções do *package* de forma habitual (importando-o com o comando `library` ou usando o operador `::` para referenciar funções do *namespace* do pacote).

**Observação.:** Caso o pacote já esteja instalado localmente e queiras atualizá-lo, utilize o mesmo comando acima com o parâmetro `force = TRUE` para forçar a atualização.

## Build local

O fluxo padrão de conferências, testes e builds encontra-se no arquivo `run_production.R`. Utilizando os comandos presentes neste _em uma sessão R_, é possível conferir a consistência das funções e criar um arquivo `.tar.gz` para instalação local do pacote.

Para distribuição aos usuários, só é necessário rodar os comandos até `devtools::check()`. O comando `devtools::build()` é necessário somente para testes locais, dado que o pacote `remotes` realiza os procedimentos necessários para instalação a partir do código-fonte e arquivo `NAMESPACE`.
